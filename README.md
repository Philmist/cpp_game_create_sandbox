# README #

(Squirrel+Sqrat)と(SDL2+SDL2_image+SDL2_ttf)と(OpenGL+GLEW)を使って、
C++で色々いじるためのテンプレートです。

SquirrelとSqratはサブモジュールで使用しています。

### 概要 ###

* MSYS2上で必要なライブラリ(SDL2等+GLEW)をインストールして使用するのを前提としています。

### ライセンス ###

* ライセンスはUnlicense( http://unlicense.org )です。