/*
 * main.cpp
 *
 * Entry point.
 */


// Include spdlog
#include <spdlog/spdlog.h>

// Include SDL2/OpenGL/GLEW/GLM
// Take care of order of including
// SDL->GLEW->SDL_OpenGL->GLU
#include <SDL.h>
#include <gl/glew.h>
#include <SDL_opengl.h>
#include <gl/glu.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Include standard libraries
#include <iostream>
#include <vector>
#include <memory>
#include <string>
#include <fstream>
#include <exception>

// Include user headers
#include "shader.hpp"


// consts.
struct {
	const unsigned int width = 640;
	const unsigned int height = 480;
} window_size;


// Global variables
static SDL_Window* gWindow;
static SDL_GLContext gGLContext;
static GLuint gVertexArrayID;

// Triangle
static const GLfloat gVertexBufferData[] = {
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
};

// Shaders
const std::string VertexShader =
"#version 330 core\n"
"layout(location = 0) in vec3 vertexPosition_modelspace;\n"
"uniform mat4 MVP;\n"
"void main() {\n"
"	gl_Position = MVP * vec4(vertexPosition_modelspace, 1);"
"}\n";
const std::string FragmentShader =
"#version 330 core\n"
"out vec3 color;\n"
"void main() {\n"
"	color = vec3(1,0,0);\n"
"}\n";

// Initialize SDL and OpenGL and etc.
bool initialize_SDL() noexcept {

	// Get logger
	auto logger = spdlog::get("main_loop");

	logger->info("Initialize SDL");

	// Init SDL
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		logger->error("SDL Init Error: {}", SDL_GetError());
		return false;
	}

	// Set OpenGL Attribute
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	// Open Window
	gWindow = SDL_CreateWindow("Hello World!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_size.width, window_size.height, (SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN));
	if (gWindow == nullptr) {
		logger->error("SDL_CreateWindow Error: {}", SDL_GetError());
		SDL_Quit();
		return false;
	}

	// Create OpenGL Context
	gGLContext = SDL_GL_CreateContext(gWindow);

	// Set Window Refresh rate to VSYNC
	SDL_GL_SetSwapInterval(1);

	// Init GLEW
	GLenum glue_error;
	glewExperimental = true;
	glue_error = glewInit();
	if (glue_error != GLEW_OK) {
		logger->error("GLEW Init Error: {}", glewGetErrorString(glue_error));
		return false;
	}

	// Create VertexArrayObject
	glGenVertexArrays(1, &gVertexArrayID);
	glBindVertexArray(gVertexArrayID);

	return true;
}


// Destroy Window and quit SDL
void deinit_SDL() {
	glDeleteVertexArrays(1, &gVertexArrayID);
	SDL_DestroyWindow(gWindow);
	gWindow = nullptr;
	SDL_Quit();
}


// Entry point
int main(int argc, char* args[]) {

	// Create logger (file)
	auto sharedFileSink = std::make_shared<spdlog::sinks::simple_file_sink_mt>("log.txt");
	auto logger = std::make_shared<spdlog::logger>("main_loop", sharedFileSink);
	spdlog::register_logger(logger);
	auto shaderLogger = std::make_shared<spdlog::logger>("shader", sharedFileSink);
	spdlog::register_logger(shaderLogger);
	logger->info("Program start.");

	// Initialize SDL and Window
	if (!initialize_SDL()) {  // if init is failed
		return -1;
	}

	// Initialize VertexBuffer
	GLuint VertexBuffer;
	glGenBuffers(1, &VertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(gVertexBufferData), gVertexBufferData, GL_STATIC_DRAW);

	// Initialize Shader
	auto ShaderProgram = amneris::Shader(VertexShader, FragmentShader);
	try {
		ShaderProgram.init();
	} catch (amneris::ShaderException e) {
		glDeleteBuffers(1, &VertexBuffer);
		logger->critical("Shader Exception: {}", e.what());
		deinit_SDL();
		return -1;
	}

	/* MAIN */
	// Clear Main window
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Create Model-View-Projection Matrix and Bind it to Shader
	GLuint MatrixId = glGetUniformLocation(ShaderProgram.get_shader_program_id(), "MVP");
	glm::mat4 Projection = glm::perspective(glm::radians(45.0f), 4.0f/3.0f, 0.1f, 100.0f);
	glm::mat4 View = glm::lookAt(
			glm::vec3(4, 3, 3),  // Camera Position in world space
			glm::vec3(0, 0, 0),  // Camera look at origin
			glm::vec3(0, 1, 0)  // Camera's head is up
			);
	glm::mat4 Model = glm::mat4(1.0f);
	auto MVP = Projection * View * Model;
	// main loop
	bool quit = false;  //! quit flag (in main loop)
	SDL_Event event;  //! SDL event structure (use in main loop)
	int is_event_in_queue;  //! SDL event in queue or not
	while (!quit) {
		is_event_in_queue = SDL_PollEvent(&event);
		if (is_event_in_queue) {
			if ((event.type == SDL_QUIT) || ((event.type == SDL_KEYDOWN) && (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE))) {
				quit = true;

			}
		}
		// Clear screen
		glClear(GL_COLOR_BUFFER_BIT);
		// Enable shader program
		ShaderProgram.start_shader_program();
		//
		glUniformMatrix4fv(MatrixId, 1, GL_FALSE, &MVP[0][0]);
		//
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
		glVertexAttribPointer(
				0,
				3,
				GL_FLOAT,
				GL_FALSE,
				0,
				(void*)0
				);
		// Draw triangle
		glDrawArrays(GL_TRIANGLES, 0, 3);
		//
		glDisableVertexAttribArray(0);
		//
		SDL_GL_SwapWindow(gWindow);
	}


	// Destroy window and some
	deinit_SDL();

	logger->info("Program finished.");
	spdlog::drop_all();

	return 0;
}

// vim: set noexpandtab sts=4 ts=4 sw=4 :
