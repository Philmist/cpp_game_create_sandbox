/*
 * shader.hpp
 *
 */
#ifndef __SHADER_HPP__
#define __SHADER_HPP__

// Include SDL and OpenGL libraries
#include <SDL.h>
#include <gl/glew.h>
#include <SDL_opengl.h>
#include <gl/glu.h>
#include <glm/glm.hpp>

// Include standard library
#include <string>
#include <stdexcept>

namespace amneris {

	using ShaderException = std::runtime_error;

	class Shader {
		public:
			Shader() = delete;
			Shader(Shader&& value) = default;
			Shader& operator=(Shader&& r_value) = default;
			Shader(const std::string& VertexShader, const std::string& FragmentShader);
			~Shader();
			void init();
			void start_shader_program();
			void stop_shader_program();
			GLuint get_shader_program_id() const { return ShaderProgramId; }
			bool is_initialized() { return initialized; }
		private:
			GLuint ShaderProgramId;
			Shader(Shader& value) = delete;
			Shader(const Shader& value) = delete;
			std::string VertexShaderString;
			std::string FragmentShaderString;
			bool initialized;
	};

}

#endif
// vim: set sts=4 ts=4 sw=4 noexpandtab :
