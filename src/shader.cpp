/*
 * shader.cpp
 *
 */

#include "shader.hpp"

// Include spdlog
#include <spdlog/spdlog.h>

#include <iostream>

namespace amneris {

	// Constructor
	Shader::Shader(const std::string& VertexShader, const std::string& FragmentShader) {
		VertexShaderString = VertexShader;
		FragmentShaderString = FragmentShader;
		initialized = false;
	}

	// Create shader program (but not start to use)
	void Shader::init() {

		// init variables
		initialized = false;

		// Get logger
		auto logger = spdlog::get("shader");

		// Create shaders
		GLuint VertexShaderId = glCreateShader(GL_VERTEX_SHADER);
		GLuint FragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
		if ((VertexShaderId == 0) || (FragmentShaderId == 0)) {
			logger->error("Cannot create Shader.");
			throw ShaderException("Cannot create shader");
		}

		// Compile vertex shader
		logger->debug("Compiling vertex shader.");
		const char* VertexSourcePointer = VertexShaderString.c_str();
		glShaderSource(VertexShaderId, 1, &VertexSourcePointer, NULL);
		glCompileShader(VertexShaderId);

		// Check Vertex Shader compile result
		logger->debug("Check vertex shader.");
		GLint vResult = GL_FALSE;
		int vInfoLogLength;
		glGetShaderiv(VertexShaderId, GL_COMPILE_STATUS, &vResult);
		glGetShaderiv(VertexShaderId, GL_INFO_LOG_LENGTH, &vInfoLogLength);
		if (vInfoLogLength > 0) {
			std::vector<char> VertexShaderErrorMessage(vInfoLogLength + 1);
			glGetShaderInfoLog(VertexShaderId, vInfoLogLength, NULL, &VertexShaderErrorMessage[0]);
			logger->info("Shader: {}", &VertexShaderErrorMessage[0]);
		}
		if (vResult == GL_FALSE) {
			glDeleteShader(VertexShaderId);
			throw ShaderException("Shader compile error.");
		}

		// Compile fragment shader
		logger->debug("Compiling fragment shader.");
		const char* FragmentSourcePointer = FragmentShaderString.c_str();
		glShaderSource(FragmentShaderId, 1, &FragmentSourcePointer, NULL);
		glCompileShader(FragmentShaderId);

		// Check Fragment Shader compile result
		logger->debug("Check fragment shader.");
		GLint fResult = GL_FALSE;
		int fInfoLogLength;
		glGetShaderiv(FragmentShaderId, GL_COMPILE_STATUS, &fResult);
		glGetShaderiv(FragmentShaderId, GL_INFO_LOG_LENGTH, &fInfoLogLength);
		if (fInfoLogLength > 0) {
			std::vector<char> FragmentShaderErrorMessage(fInfoLogLength + 1);
			glGetShaderInfoLog(FragmentShaderId, vInfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
			logger->error("Shader: {}", &FragmentShaderErrorMessage[0]);
		}
		if (fResult == GL_FALSE) {
			glDeleteShader(FragmentShaderId);
			throw ShaderException("Shader compile error.");
		}

		// Create program, Link and Attach shader
		logger->debug("Linking program.");
		GLuint ProgramId = glCreateProgram();
		if (ProgramId == 0) {
			throw ShaderException("Cannot create program");
		}
		glAttachShader(ProgramId, VertexShaderId);
		glAttachShader(ProgramId, FragmentShaderId);
		glLinkProgram(ProgramId);

		// Check the program
		GLint pResult = GL_FALSE;
		int pInfoLogLength;
		glGetProgramiv(ProgramId, GL_LINK_STATUS, &pResult);
		glGetProgramiv(ProgramId, GL_INFO_LOG_LENGTH, &pInfoLogLength);
		if (pInfoLogLength > 0) {
			std::vector<char> ProgramErrorMessage(pInfoLogLength + 1);
			glGetProgramInfoLog(ProgramId, pInfoLogLength, NULL, &ProgramErrorMessage[0]);
			logger->error("Linker: {}", &ProgramErrorMessage[0]);
		}
		if (pResult == GL_FALSE) {
			glDeleteShader(VertexShaderId);
			glDeleteShader(FragmentShaderId);
			glDeleteProgram(ProgramId);
			throw ShaderException("Cannot link shader.");
		}

		// Detach Shader
		// because shader is in program
		glDetachShader(ProgramId, VertexShaderId);
		glDetachShader(ProgramId, FragmentShaderId);

		// Mark shader (delete)
		glDeleteShader(VertexShaderId);
		glDeleteShader(FragmentShaderId);

		ShaderProgramId = ProgramId;

		initialized = true;

	}

	// Start to use(apply) shader program
	void Shader::start_shader_program() {
		glUseProgram(ShaderProgramId);
	}

	// Stop to use(apply) shader program
	void Shader::stop_shader_program() {
		glUseProgram(0);
	}

	// dtor.
	Shader::~Shader() {

		glDeleteProgram(ShaderProgramId);

	}
}

// vim: set sts=4 ts=4 sw=4 noexpandtab :
